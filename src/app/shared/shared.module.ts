import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { ApiService } from './services/api.service';
import { RouterModule } from '@angular/router';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { BsDropdownModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TooltipModule,
    HttpModule,
    FormsModule,
    BsDropdownModule.forRoot(),
  ],
  declarations: [FooterComponent, HeaderComponent],
  exports: [HeaderComponent, FooterComponent],
  providers: [ApiService,AuthService]
})
export class SharedModule { }
