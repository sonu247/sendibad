export interface IBlogModel{
    categories:IBlogCategoryModel[]
}
export interface IBlogCategoryModel{
    id:string;
    category_name:string;
}