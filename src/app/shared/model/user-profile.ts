export interface IUserProfileModel{
    doc_type_list:IUserProfileDocTypeModel[];
}
    
export interface IUserProfileDocTypeModel{
    id:string;
    name:string;
}