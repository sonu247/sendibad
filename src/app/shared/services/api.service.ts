import { Injectable } from '@angular/core';
import {Http, Response, RequestOptions, Headers} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import "rxjs/Rx"
import { AppConfig } from '../../config/app.config';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _hosts = AppConfig.API_ENDPOINT;

  constructor(private http: Http) { }

  get = (url:string):Observable<any> =>{
    var requestUrl: string = this._hosts+url;
    let options = new RequestOptions({headers:new Headers({'Auth-Key':localStorage.getItem('token')})})
    return this.http.get(requestUrl,options).map(res => res.json());
  }
 post(url: string, body?:any, host?:String) : Observable<any>{
    host = host ? host : this._hosts;
    let options = new RequestOptions({headers:new Headers({'Auth-Key':localStorage.getItem('token')})})
    var formdata = new FormData();
    for(var i in body){
    formdata.append(i,body[i]);
    }
    console.log(formdata);
    return this.http.post( host + url, formdata,options).map((res: Response)=>res.json());
   }
  put = (url: string, body?:Object, host?:String) : Observable<any> =>{
    host = host ? host : this._hosts;
    var formdata = new FormData();
    for(var i in body){
    formdata.append(i,body[i]);
    }
    return this.http.put( host + url,formdata).map((res: Response)=>res.json());
   }
   patch = (url: string, body?:Object, host?:String) : Observable<any> =>{
    host = host ? host : this._hosts;
    var formdata = new FormData();
    for(var i in body){
    formdata.append(i,body[i]);
    }
    return this.http.patch( host + url,formdata).map((res: Response)=>res.json());
   }
}
