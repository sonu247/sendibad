import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  isLoggedIn: boolean = false;
  constructor(private router: Router) { }

  updateLoginStatus = (loginStatus: boolean) => { 
    this.isLoggedIn = loginStatus;

  }
  
  checkLogin = () => {
    try {
      const token = localStorage.getItem('token');
      if(token){
        this.isLoggedIn = true;
      }else{
        this.router.navigate(['/']);
      }
    } catch (error) {
      this.isLoggedIn = false;
      this.router.navigate(['/']);
    }
  }
  
}