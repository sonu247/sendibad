import { Component, OnInit } from '@angular/core';
import { Router,Event, NavigationEnd } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  homePageComponent: boolean = false;
  whiteListUrls: Array<string> = [
    '/',
    '/auth/login',
    '/auth/signup',
    '/auth/forgot-password',
    '/auth/reset-password'
  ];
  constructor(private router:Router,private authServe: AuthService) { }

  ngOnInit() {
    this.router.events.subscribe((event: Event) => { 
        if (event instanceof NavigationEnd ) {
          let url = this.router.url;
          if(this.whiteListUrls.indexOf(url) > -1){
            
          }else {
            let isBlackListed: boolean = true;
            for (let i = 0; i < this.whiteListUrls.length; i++) {
              const ele = this.whiteListUrls[i];
              if(ele!='/'&& url.indexOf(ele) > -1){
                isBlackListed = false;
              }
            };
            if(isBlackListed){
              this.authServe.checkLogin();
            }
        }
          if(url == '/'){
            this.homePageComponent = true
          }else{
            this.homePageComponent = false;
          }
        }
    })
  }
  logout = () =>{
    const res = confirm('Are you sure ?');
    if(!res){
      return;
    }
    localStorage.clear();
    this.authServe.updateLoginStatus(false);
    this.router.navigate(['/auth/login']);   
  }
}
  