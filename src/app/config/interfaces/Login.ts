export interface ILogin {
    isLogginIn: Boolean;
    loginData: ILoginDetails;
    isError?: Boolean;
    isSuccess?: Boolean;
}

export interface ILoginDetails {
    email: String;
    password: String;
}
