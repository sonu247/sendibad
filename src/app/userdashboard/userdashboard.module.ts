import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { userdashboardRoutes } from './userdashboard.routes';
import { UserdashboardComponent } from './userdashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { PackagesComponent } from './packages/packages.component';
import { MessagesComponent } from './messages/messages.component';
import { CustomerServiceComponent } from './customer-service/customer-service.component';
import { UserTripComponent } from './user-trip/user-trip.component';
import { DashboardSidebarComponent } from './dashboard-sidebar/dashboard-sidebar.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(userdashboardRoutes),
    TooltipModule.forRoot(),
    FormsModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [UserdashboardComponent, UserProfileComponent, PackagesComponent, MessagesComponent, CustomerServiceComponent, UserTripComponent, DashboardSidebarComponent]
})
export class UserdashboardModule { }
