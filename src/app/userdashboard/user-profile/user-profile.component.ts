import { Component, OnInit } from '@angular/core';
import { PhoneCodes as countryCodes, AllCountries as allCountries } from '../../shared/Data/Phone';
import { ApiService } from '../../shared/services/api.service';
import { IUserProfileDocTypeModel } from '../../shared/model/user-profile';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  selectedCountry: string = "IN"
  allCountryPhoneCode:Object = countryCodes;
  allCountryCode:Object = allCountries;
  phoneCode:string;
  userData:User={
    first_name:"",
    last_name:"",
    mobile:"",
    id_doc_type:"",
    id_number:"",
    expiry_date:"",
    country:""
  }
  documentTypeData:IUserProfileDocTypeModel[] = DoucumentTypeDetails;
  constructor(private apiServe: ApiService) { }

  ngOnInit() {
    this.getUserProfileData();
  }
    updatePhoneCode = () =>{
      this.selectedCountry = this.userData.country;
      this.phoneCode = this.allCountryPhoneCode[this.selectedCountry];
    }
  getUserProfileData = () =>{
    this.apiServe.get('user_profile').subscribe(data =>{
      this.userData = data.user;
      this.documentTypeData = data.doc_type_list as IUserProfileDocTypeModel[];
      console.log(this.documentTypeData);
    })
  }

}
const DoucumentTypeDetails: IUserProfileDocTypeModel[] = [];
interface User{
  first_name:string,
  last_name:string,
  mobile:string,
  id_doc_type:string,
  id_number:string,
  expiry_date:string,
  country:string
}
  