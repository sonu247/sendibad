import { Routes } from '@angular/router';
import { UserdashboardComponent } from './userdashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CustomerServiceComponent } from './customer-service/customer-service.component';
import { MessagesComponent } from './messages/messages.component';
import { PackagesComponent } from './packages/packages.component';
import { UserTripComponent } from './user-trip/user-trip.component';
export const userdashboardRoutes: Routes = [
    {
        path:'',
        component:UserdashboardComponent,
        children: [
            {
                path:'',
                component:UserProfileComponent
            },
            {
                path:'profile',
                component:UserProfileComponent
            },
            {
                path:'customer-service',
                component:CustomerServiceComponent
            },
            {
                path:'messages',
                component:MessagesComponent
            },
            {
                path:'packages',
                component:PackagesComponent
            },
            {
                path:'my-trip',
                component:UserTripComponent
            },
            
        ]
    }
]
