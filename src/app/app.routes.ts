import { Routes } from '@angular/router';

export const appMainRoutes: Routes = [
    {
        path: '',
        loadChildren: './home/home.module#HomeModule'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
    },
    {
        path:'dashboard',
        loadChildren: './userdashboard/userdashboard.module#UserdashboardModule'
    },
    {
        path:'blogs',
        loadChildren: './blog/blog.module#BlogModule'
    }
];
