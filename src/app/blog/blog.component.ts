import { Component, OnInit } from '@angular/core';
import {  IBlogCategoryModel } from '../shared/model/blog';
import { ApiService } from '../shared/services/api.service';
import { TagModel } from 'ngx-chips/core/accessor';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  categoriesData:IBlogCategoryModel[] = CategoriesData;
  items = [];
  blogData: BlogContent={
    blog_title:"",
    blog_content:"",
    blog_small_content:"",
    blog_category:"",
    featured_image:""
  }
  public editorValue: string = '';
  url: string;
  displayImage: boolean = false;
  constructor(private apiServe: ApiService,) { }

  ngOnInit() {
      this.getCategoriesData();
  }
  getCategoriesData = () =>{
      this.apiServe.get('category/').subscribe(data =>{
      this.categoriesData = data.category;
      console.log(this.categoriesData)
    })
  }
  addBlog = () =>{
    let tags = [];
    for(let i=0; i < this.items.length; i++){
      const currentElement=  this.items[i];
      tags.push(currentElement.value);
    }
    console.log(tags);
    this.apiServe.post('add_blog',{ ...this.blogData, blog_tag: tags}).subscribe(data =>{

    })
  }
  public onAdding(tag: TagModel): Observable<TagModel> {
    const confirm = window.confirm('Do you really want to add this tag?');
    return Observable
        .of(tag)
        .filter(() => confirm);
  }
  public onRemoving(tag: TagModel): Observable<TagModel> {
    const confirm = window.confirm('Do you really want to remove this tag?');
    return Observable
        .of(tag)
        .filter(() => confirm);
  }

  uploadFile = (event:any) =>{
    this.blogData.featured_image = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.url = event.target.result;
        this.displayImage = true;
      }
      this.displayImage = false;
      reader.readAsDataURL(event.target.files[0]);
   }   
  }
}
const CategoriesData: IBlogCategoryModel[] = [];
interface BlogContent{
    blog_title:string;
    blog_content:string;
    blog_small_content:string;
    blog_category:string;
    featured_image:string
}