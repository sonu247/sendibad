import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { blogRoutes } from './blog.routes';
import { BlogComponent } from './blog.component';
import { TagInputModule } from 'ngx-chips';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { CKEditorModule } from 'ngx-ckeditor';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(blogRoutes),
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule
  ],
  declarations: [
    BlogComponent
  ]
})
export class BlogModule { }
