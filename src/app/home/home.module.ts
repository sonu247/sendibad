import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { homeRoutes } from './home.routes';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { SlickModule } from 'ngx-slick';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(homeRoutes),
    CarouselModule.forRoot(),
    SlickModule.forRoot()
  ],
  declarations: [HomeComponent]
})
export class HomeModule { 
 myInterval = 100;
}
