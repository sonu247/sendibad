import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  userData:User={
    email:""
  }
  constructor(private apiServe: ApiService) { }

  ngOnInit() {}

  forgotPassword = () =>{
    this.apiServe.post('forgot_password',this.userData).subscribe(data =>{})
  }
}
interface User{
  email:string;
}

