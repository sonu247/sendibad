import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ApiService } from '../../shared/services/api.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  userData:User={
    password:"",
    confirm_password:"",
    token:""
  }
  token:object;
  constructor(private activatedRoute: ActivatedRoute,private apiServe: ApiService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) =>{
       this.verifyLink(params);
       this.token = params.token;
    });
    console.log(this.token)
  }
  verifyLink = (params: object)=>{
    this.apiServe.post('reset_password_check',params).subscribe(data =>{
    })
  }
  resetPassword = () =>{
    this.apiServe.post('reset_password',{password:this.userData.password,confirm_password:this.userData.confirm_password,token:this.token}).subscribe(data =>{})
  }

}
interface User{
  password:string;
  confirm_password:string;
  token:string;
}