import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  userData: User ={
    first_name:"",
    last_name:"",
    email:"",
    password:"",
    confirm_password:""
  }
  constructor( private apiServe: ApiService,private router: Router) {}

  
  ngOnInit() {
    }
  userSignup = () =>{
    this.apiServe.post('signup',this.userData).subscribe(data =>{
      if(data.message){
        this.router.navigate(['/dashboard']);      }
    })
  }
}
interface User {
  first_name:string;
  last_name:string;
  email:string;
  password:string;
  confirm_password:string;
}
