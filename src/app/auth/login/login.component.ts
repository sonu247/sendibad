import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  userData:User={
    email:"",
    password:""
  }
  isDataIncorrect: boolean = false;
  erroMessage:string;
  constructor(private apiServe: ApiService,private router: Router) {}

  ngOnInit() {
  }
  userLogin = () =>{
    this.apiServe.post('login',this.userData).subscribe(data =>{
      if(data.token){
        localStorage.setItem('token', data.token);
        this.router.navigate(['/dashboard']);
      }
      if(data.message){
        this.isDataIncorrect = true;
        this.erroMessage = data.message;
      }
    })
  }

}
interface User{
  email:string;
  password:string;
}
